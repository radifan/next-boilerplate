import React from 'react';
import { intlShape } from 'react-intl';
import { Header } from 'semantic-ui-react';
import { compose } from 'redux';
import 'semantic-ui-css/semantic.min.css';

import DefaultLayout from 'layouts/DefaultLayout';
import Section from 'components/Section';
import withReduxSaga from 'utils/withReduxSaga';
import withIntl from 'utils/withIntl';
import messages from 'locale/messages/home';

class Homepage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <DefaultLayout transparent>
        <Section textAlign="center" minHeight={700} verticallyCentered>
          <Header size="huge">
            {this.props.intl.formatMessage(messages.title)}
          </Header>
        </Section>
      </DefaultLayout>);
  }
}

Homepage.propTypes = {
  intl: intlShape.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

export default compose(
  withReduxSaga(mapStateToProps, mapDispatchToProps),
  withIntl,
)(Homepage);
