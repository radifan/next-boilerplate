import withRedux from 'next-redux-wrapper';
import nextReduxSaga from 'next-redux-saga';
import { configureStore } from 'store/configureStore';

export default function withReduxSaga(...connectArgs) {
  return (BaseComponent) => withRedux(configureStore, ...connectArgs)(nextReduxSaga(BaseComponent));
}
