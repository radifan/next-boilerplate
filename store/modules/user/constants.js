export const LOAD_USERS = 'user/LOAD_USERS';
export const LOAD_USERS_SUCCESS = 'user/LOAD_USERS_SUCCESS';
export const LOAD_USERS_FAILED = 'user/LOAD_USERS_FAILED';
