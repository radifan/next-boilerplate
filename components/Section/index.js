import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Container } from 'semantic-ui-react';

const Section = ({
  children, textAlign, minHeight, verticallyCentered, backgroundColor, backgroundImage,
}) => {
  let vertical = {};
  let background = {};

  if (verticallyCentered) {
    vertical = {
      display: 'flex',
      alignItems: 'center',
    };
  }

  if (backgroundImage) {
    background = {
      backgroundImage: `url('${backgroundImage}')`,
      backgroundSize: 'cover',
      backgroundPosition: 'center center',
      backgroundRepeat: 'no-repeat',
    };
  }

  return (
    <Segment
      style={{
        minHeight, backgroundColor, ...vertical, ...background,
      }}
      vertical
      basic
    >
      <Container textAlign={textAlign}>
        {children}
      </Container>
    </Segment>
  );
};

Section.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  textAlign: PropTypes.string,
  minHeight: PropTypes.number,
  verticallyCentered: PropTypes.bool,
  backgroundColor: PropTypes.string,
  backgroundImage: PropTypes.string,
};

export default Section;
