import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'next/router';
import { Breadcrumb as BreadcrumbComponent } from 'semantic-ui-react';

const Breadcrumb = ({ router }) => {
  let links = router.pathname.split('/');
  links = links.filter((link) => link !== '');

  const handleBreadcrumbClick = (e, { href }) => {
    e.preventDefault();
    router.push(href);
  };

  return (
    <div className="breadcrumb-container">
      <BreadcrumbComponent>
        <BreadcrumbComponent.Section onClick={handleBreadcrumbClick} href="/" link>Home</BreadcrumbComponent.Section>
        <BreadcrumbComponent.Divider />
        {links.length > 0 && links.map((link, index) => {
          const routeName = (
            <span className="route-name">
              {link}
            </span>
          );

          if (index === (links.length - 1)) {
            return (
              <BreadcrumbComponent.Section active>
                {routeName}
              </BreadcrumbComponent.Section>
            );
          }

          return [
            <BreadcrumbComponent.Section href={link} link>
              {routeName}
            </BreadcrumbComponent.Section>,
            <BreadcrumbComponent.Divider />,
          ];
        })}
      </BreadcrumbComponent>
      <style jsx>
        {`
          .breadcrumb-container {
            position: absolute;
            right: 0;
            top: 25%;
          }

          .route-name {
            text-transform: capitalize;
          }
        `}
      </style>
    </div>
  );
};

Breadcrumb.propTypes = {
  router: PropTypes.any,
};

export default withRouter(Breadcrumb);
