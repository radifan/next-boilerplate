import { StyleSheet } from 'aphrodite';

export default StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
  },
  innerContainer: {
    display: 'flex',
    paddingTop: '20px',
    paddingBottom: '20px',
  },
});
