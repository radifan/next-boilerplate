import React from 'react';
import PropTypes from 'prop-types';
import { Segment, Container, Header } from 'semantic-ui-react';

import Breadcrumb from '../Breadcrumb';

const PageTitle = ({ title, subtitle }) => (
  <div>
    <Segment style={{ backgroundColor: '#f5f5f5' }} basic>
      <Container style={{ paddingTop: 20, paddingBottom: 20 }}>
        <div className="pagetitle-container">
          <div className="pagetitle-header">
            <Header as="h3">
              {title.toUpperCase()}
              <Header.Subheader>
                {subtitle}
              </Header.Subheader>
            </Header>
          </div>
          <Breadcrumb />
        </div>
      </Container>
    </Segment>
    <style jsx>
      {`
        .pagetitle-container {
          width: 100%;
          position: relative;
        }

        .pagetitle-header {
          margin-bottom: 0;
        }
      `}
    </style>
  </div>
);

PageTitle.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
};

export default PageTitle;
